    $(document).ready(function(){
      // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
      $('.modal').modal();
    });

    //botonera
    $( "#uno" ).click(function() {
      $("#rut").val($('#rut').val()+'1');
    });
    $( "#dos" ).click(function() {
      $("#rut").val($('#rut').val()+'2');
    });
    $( "#tres" ).click(function() {
      $("#rut").val($('#rut').val()+'3');
    });
    $( "#cuatro" ).click(function() {
      $("#rut").val($('#rut').val()+'4');
    });
    $( "#cinco" ).click(function() {
      $("#rut").val($('#rut').val()+'5');
    });
    $( "#seis" ).click(function() {
      $("#rut").val($('#rut').val()+'6');
    });
    $( "#siete" ).click(function() {
      $("#rut").val($('#rut').val()+'7');
    });
    $( "#ocho" ).click(function() {
      $("#rut").val($('#rut').val()+'8');
    });
    $( "#nueve" ).click(function() {
      $("#rut").val($('#rut').val()+'9');
    });
    $( "#cero" ).click(function() {
      $("#rut").val($('#rut').val()+'0');
    });
    $( "#ka" ).click(function() {
      $("#rut").val($('#rut').val()+'k');
    });
    $( "#del" ).click(function() {
      var result = $("#rut").val();
      var inp = mString = result.substring(0,result.length-1) ;
      $("#rut").val(inp);
    });
    //imprime ticket
    $( ".imprime" ).click(function() {
      // $('.printable').css({width:'500px'});
      window.print();
    });
    //mostrar mapa
    $( "#btn-mapa" ).click(function() {
      $('#mapa_iframe').css({display:''});
      $('.no_map').css({display:'none'});
    });
    //cerrar mapa
    $( "#cerrar_mapa" ).click(function() {
      $('#mapa_iframe').css({display:'none'});
      $('.no_map').css({display:''});
    });

    $("#rut").change(function () {

    })

    jQuery(document).ready(function($){
      $('.input_rut').rut();
    });

    // click and hold event listener

    var timeout_id = 0,
        hold_time = 1000,
        hold_trigger = $('#del');


    hold_trigger.mousedown(function() {
        timeout_id = setTimeout(clean, hold_time);
    }).bind('mouseup mouseleave', function() {
        clearTimeout(timeout_id);
    });

    function clean() {
      $("#rut").val('');
    }


  $( "#boton" ).click(function() {
    var irut = $("#rut").val();
    var formateado = $.rut.formatear(irut);
    console.info('formateado');
    $("#rut").val(formateado);
    $( "#form" ).submit();
  })

  $( "#form" ).submit(function( event ) {
    var irut = $("#rut").val();
    var es_valido = $.rut.validar(irut);
    var rut_sd = $("#rut").val().split('-');
    rut = rut_sd[0];
    // die();
    if ( es_valido ) {
      console.info('antes de entrar al ajax');
      $.ajax({
              type: "GET",
              dataType: 'jsonp',
              data: { get_param: '0' }, 
              url: "http://192.168.5.99/apirest/demo/"+rut+"?X-API-KEY=123456",
              success: function (data) { 
                  console.info(data);
                  $('.nombre').html(data.response[0].NOMBRE_PACIENTE + ' ' + data.response[0].APEPAT_PACIENTE + ' ' + data.response[0].APEMAT_PACIENTE);
                  $('.ficha').html(data.response[0].NUM_FICHA);
                  $('#modal_paciente').modal('open');
                  $("#rut").val('');
                  // window.print();

              },
              error: function(error) {
                  $('#modal_error').modal('open');
                  $('#mensaje_error').html('Paciente no encontrado');
              }
      });
      // $('#modal_error').modal('open');

      return;
    }
   
    $('#modal_error').modal('open');
    // $( "span" ).text( "Not valid!" ).show().fadeOut( 1000 );
    event.preventDefault();
  });

  // $(document).ready(function() {
  //     var yetVisited = localStorage['visited'];
  //     // localStorage['visited'] = "no";
  //     alert(localStorage['visited']);
  // });